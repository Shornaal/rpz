# include <psocks.h>
# include "libft/libft.h"
int	main(void)
{
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == SOCKET_INVALID)
	{
		ft_putstr("Error on socket initialization.");
		return(-1);
	}
	struct	hostent	*hostinfo;
	SOCKADDR_IN	sin;
	const char	*hostname = "www.developpez.com";
	
	hostinfo = gethostbyname(hostname);
	if (!hostinfo)
	{
		ft_putstr("Unknow host");
		return (-1);
	}
	sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
	sin.sin_port = htons(PORT);
	sin.sin_family = AF_INET;
	if (connect(sock, (SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
	{
		ft_putstr("Error on connection to remote host");
		return (-1); 
	}
	ft_putstr("Tout les systèmes, okay !");
	return (0);
}
