#ifndef H_LIBFT_H
# define H_LIBFT_H
# include <unistd.h>
# include <stdlib.h>
void	ft_putchar(char c);
void	ft_putstr(const char *str);
void	ft_putnbr(int nb);
void	ft_swap(int *a, int *b);
int	ft_iswhitespace(char c);
int	ft_isdigit(char c);
int	ft_isprime(int nb);
int	ft_str_is_printable(const char *str);
int	ft_atoi(char *str);
int	ft_sqrt(int nb);
int	ft_fibonacci(int nb);
int	ft_strlen(const char *str);
int	ft_strcmp(char *s1, char *s2);
int	ft_strncmp(char *s1, char *s2, unsigned int n);
char 	*ft_strrev(char *str);
char	*ft_strcpy(char *src, char *dest);
char	*ft_strncpy(char *dest, char *src, unsigned int n);
char	*ft_strstr(char *str, char *to_find);
char	*ft_strdup(char *str);
#endif
