#ifndef FT_CAA_H
#define FT_CAA_H
# include <unistd.h>
# include <stdlib.h>
# define TILE_WALL 	1
# define TILE_FLOOR 	0
/*
**	~~ Map
*/
typedef struct		s_map
{
	int		heigth;
	int		width;
	int		**tiles;
}			t_map;

t_map	*ft_map_initialize(int heigth, int width);
void	ft_map_free(t_map *map);
/*
** 	~~MapGenerator
*/
typedef struct 		s_mapgen
{
	int		wall_density;
	struct s_map	*map;
}			t_mapgen;

t_mapgen	ft_mapgen_initialize(int wall_density, t_map *map);
void		ft_mapgen_generate(t_mapgen *map_gen);
void		ft_mapgen_free(t_mapgen *map_gen);
/*
**	~~ ft_lib
*/
void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putnbr(int nbr);
void	ft_putmap(t_map *map, int wall_density);
void	ft_perror(const char *s);
int	ft_atoi(const char *s);
/*
**	~~ Error handling
*/
int	ft_valid_args(int argc, char **argv);
#endif
