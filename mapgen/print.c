#include <caa.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		write(1, str + i, 1);
		i++;
	}
}

void	ft_perror(const char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		write(2, str + i, 1);
		i++;
	}
}

void	ft_putnbr(int nbr)
{
	if (nbr < 0)
		nbr = -nbr;
	if (nbr >= 10)
	{
		ft_putnbr(nbr / 10);
		ft_putnbr(nbr % 10);
	}
	else
	{
		ft_putchar('0' + nbr);
	}	
}

void	ft_putmap(t_map *map, int wall_density)
{
	int	i;
	int	j;

	i = 0;
	ft_putstr("Heigth:");
	ft_putnbr(map->heigth);
	ft_putstr("\tWidth:");
	ft_putnbr(map->width);
	ft_putstr("\t%Wall:");
	ft_putnbr(wall_density);
	ft_putchar('\n');
	while (i < map->heigth)
	{
		j = 0;
		while (j < map->width)
		{
			(map->tiles[i][j] == TILE_WALL) ? ft_putchar('X') : ft_putchar('.');
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}
