#include <caa.h>

int	ft_atoi(const char *s)
{
	int	nb;
	int	neg;
	int	i;

	i = 0;
	nb = 0;
	neg = 1;

	while (s[i] == ' ')
		i++;
	neg = (*s == '-' ? -1 : 1);
	while ((s[i] >= '0' && s[i] <= '9') && s[i] != '\0')
	{
		nb = nb * 10 + (s[i] - '0');
		i++;
	}
	return (nb * neg);
}

int	ft_valid_args(int argc, char **argv)
{
	int	i;
	int	j;

	if (argc != 4)
		return (0);
	i = 1;
	while (i < 4)
	{
		j = 0;
		while (argv[i][j] != '\0')
		{
			if (!(argv[i][j] >= '0' && argv[i][j] <= '9'))
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int	main(int argc, char **argv)
{
	/*
	** @argv
	** Hauteur, largeur, densitée
	** @action
	** dispatch to map generator
	*/
	t_mapgen	map_gen;
	t_map		*map;

	if (!ft_valid_args(argc, argv))
	{
		ft_perror("Arguments given to the program was invalid for some reasons. Please retry.\n");
		return (0);
	}
	map = ft_map_initialize(atoi(argv[1]), ft_atoi(argv[2]));
	map_gen = ft_mapgen_initialize(ft_atoi(argv[3]), map);
	ft_mapgen_generate(&map_gen);
	ft_putmap(map_gen.map, map_gen.wall_density);
	ft_putstr("Alors les jeunes, ça segfault ?\n");
	ft_mapgen_free(&map_gen);
	ft_putchar('\n');
	return (1);
}
