#include <caa.h>

t_map	*ft_map_initialize(int heigth, int width)
{
	t_map	*map;
	int	i;
	int	j;

	map = (t_map *)malloc(sizeof(struct s_map));
	map->heigth = heigth;
	map->width = width;
	map->tiles = (int **)malloc(sizeof(int *) * heigth);
	if (map->tiles == NULL)
		return (map);
	i = 0;
	while (i < heigth)
	{
		map->tiles[i] = (int *)malloc(sizeof(int) * width);
		if (map->tiles[i] == NULL)
			return (map);
		j = 0;
		while (++j < width)
			map->tiles[i][j] = 0;
		i++;
	}
	return (map);
}

void	ft_map_free(t_map *map)
{
	int	i;

	i = 0;
	while (i < map->heigth)
	{
		free(map->tiles[i]);
		i++;
	}
	free(map->tiles);
}
