#include <caa.h>

t_mapgen	ft_mapgen_initialize(int wall_density, t_map *map)
{
	t_mapgen	map_gen;
	
	map_gen.wall_density = wall_density;
	map_gen.map = map;
	return (map_gen);
}

void	ft_mapgen_generate(t_mapgen *map_gen)
{
	int	rand;
	int	i;
	int	j;

	i = 0;
	while (i < map_gen->map->heigth)
	{
		j = 0;
		while (j < map_gen->map->width)
		{
			rand = random() % 100;
			if (rand < map_gen->wall_density)
				 map_gen->map->tiles[i][j] = 1;
			else
				 map_gen->map->tiles[i][j] = 0;
			j++;
		}
		i++;
	}
}

void	ft_mapgen_free(t_mapgen *map_gen)
{
	map_gen->wall_density = 0;
	ft_map_free(map_gen->map);
	free(map_gen->map);
}
