#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#define true 1
#define false 0
// Screen dimension constants
const int	SCREEN_WIDTH = 640;
const int	SCREEN_HEIGHT = 480;
typedef	int	bool;

int	main(void)
{
	SDL_Window 	*window;
	SDL_Surface 	*screenSurface;
	SDL_Event	event;
	SDL_Surface	*bitmap;
	bool		running;

	window = NULL;
	screenSurface = NULL;
	bitmap = NULL;
	running = true;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		printf("SDL could not initialize SDL_error: %s\n", SDL_GetError());
	else
	{
		window = SDL_CreateWindow("SDL To High", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
			printf("Window could not be created ! SDL_Error %s\n", SDL_GetError());
		screenSurface = SDL_GetWindowSurface(window);
		bitmap = SDL_LoadBMP("sdl_logo.bmp");
		while (running)
		{
			while(SDL_PollEvent(&event) != 0)
			{
				if (event.type == SDL_QUIT)
				{
					running = false;
				}
			}
			SDL_BlitSurface(bitmap, NULL, screenSurface, NULL);
			SDL_UpdateWindowSurface(window);
		}
		SDL_DestroyWindow(window);
		SDL_Quit();
	}
	return (0);
}
